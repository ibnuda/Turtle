﻿namespace Turtle.Ruting

open FParsec
open Route
open System
open System.Text
open Microsoft.FSharp.Core.CompilerServices
open Microsoft.FSharp.Collections
open System.Collections.Generic
open System.Diagnostics
open System.IO
open System.Reflection

module RouteCompilation =
  type RouteCompilationArgs = {
    TypeName: string
    Parse: Route list
    InputType: bool
    ReturnType: bool
    NameSpace: string option
    ModuleName: string option
    OutputType: CompilationOutputType 
  } and CompilationOutputType = | FSharp

  type RouterKlass = {
    Name: string
    Ctor: HandlerCtorParam list option
    RouteKlasses: RouterBuilder list
    Methods: Method list
  } and RouterBuilder = {
    Name: string
    Arguments: FunctionParam list
    Segments: NamedRouteSegment list
  } and DynamicParam =
    | Int64Param of string
    | IntParam of string
    | StringParam of string
    | GuidParam of string
  and FunctionParam = | FunctionParam of DynamicParam
  and HandlerCtorParam = {
    Name: string
    HandlerArgs: FunctionParam list
  } and Method = {
    Name: string
    FunctionParams: FunctionParam list
  }

  let routeName (route: Route): string =
    match route.RouteName with
    | Some routeName -> routeName
    | None ->
      let routeParts = route.RouteSegments |> List.choose (function
        | ConstantSeg name -> Some name
        | _ -> None
      )
      sprintf "%s__%s" (route.Verb) (String.concat "_" routeParts)

  let routeIVars (route: Route): FunctionParam list =
    List.choose (function
      | ConstantSeg _ -> None
      | Int64Seg name -> Some (FunctionParam (Int64Param name))
      | IntSeg name -> Some (FunctionParam (IntParam name))
      | StringSeg name -> Some (FunctionParam (StringParam name))
      | GuidSeg name -> Some (FunctionParam (GuidParam name)))
      (route.RouteSegments)

  let routeSubClass (route: Route): RouterBuilder = {
    Name = (routeName route)
    Arguments = (routeIVars route)
    Segments = route.RouteSegments
  }

  let makeRouteKlasses (routes: Route list): RouterBuilder list =
    List.map routeSubClass routes

  let handlerName: Route -> string = routeName

  let makeHandlerCtorParam (route: Route): HandlerCtorParam = {
    Name = (handlerName route)
    HandlerArgs =
      (List.choose (function
        | Int64Seg name -> Some (FunctionParam (Int64Param name))
        | IntSeg name -> Some (FunctionParam (IntParam name))
        | StringSeg name -> Some (FunctionParam (StringParam name))
        | GuidSeg name -> Some (FunctionParam (GuidParam name))
        | ConstantSeg _ -> None)
        (route.RouteSegments))
  }

  let makeCtor (routes: Route list): HandlerCtorParam list option =
    Some ((routes |> List.map makeHandlerCtorParam))

  let route2Class (routes: Route list) (options: RouteCompilationArgs) = {
    Name = options.TypeName
    Ctor = (makeCtor routes)
    RouteKlasses = (makeRouteKlasses routes)
    Methods = []
  }

  type FSharpWriter (indentSize: int) =
    let mutable indentation: int = 0
    let mutable lastNewLine: bool = false
    member val IndentSize = indentSize with get
    member val Content = new StringBuilder() with get
    member x.IncIndent () = indentation <- indentation + x.IndentSize
    member x.DecIndent () = indentation <- indentation - x.IndentSize
    member x.Indent () =
      x.IncIndent ()
      { new System.IDisposable with
          member y.Dispose () = x.DecIndent ()
      }

    member x.StartWriteLine (line: string) =
      let pfx: string = new string (' ', indentation)
      let s: string = sprintf "%s%s\n" pfx line
      if s.Length > 0 then lastNewLine <- s.[s.Length - 1] = '\n'
      x.Content.Append(s) |> ignore

    member x.StartWrite (line: string) =
      let pfx: string = new string (' ', indentation)
      let s: string = sprintf "%s%s" pfx line
      if s.Length > 0 then lastNewLine <- s.[s.Length - 1] = '\n'
      x.Content.Append(s) |> ignore

    member x.Write (str: string) =
      if str.Length > 0 then lastNewLine <- str.[str.Length - 1] = '\n'
      x.Content.Append(str) |> ignore

    member x.Newline () =
      if not lastNewLine then
        x.Write ("\n")
        lastNewLine <- true

  let idVal: IdentifierValidator = new FParsec.IdentifierValidator ()

  let quoteIfNeeded (fnName: string): string =
    let s, _ = idVal.ValidateAndNormalize (fnName)
    match s with
    | null -> sprintf "``%s``" s
    | _ -> s

  let renderRouteBuilder (klass: RouterBuilder) (writer: FSharpWriter) =
    writer.StartWrite <| sprintf "let %s " (quoteIfNeeded <| klass.Name)
    using (writer.Indent()) <| fun _ ->
      let argStrings =
        klass.Arguments
        |> List.map (function
          | FunctionParam (Int64Param name) -> sprintf "(%s:int64)" (quoteIfNeeded name)
          | FunctionParam (IntParam name) -> sprintf "(%s:int)" (quoteIfNeeded name)
          | FunctionParam (StringParam name) -> sprintf "(%s:string)" (quoteIfNeeded name)
          | FunctionParam (GuidParam name) -> sprintf "(%s:Guid)" (quoteIfNeeded name)
        )
      let argString = argStrings |> String.concat " "
      writer.Write <| sprintf "%s = \n" argString

      using (writer.Indent()) (fun _ ->
        let rec makeRouteBuilder segs (tmpConsts: ResizeArray<_>) (ret: ResizeArray<_>) =
          match segs with
          | [] ->
            if tmpConsts.Count > 0 then
              let suffix = tmpConsts |> String.concat "/" |> sprintf "\"%s/\""
              ret.Add suffix
            ret |> String.concat " + "
          | ConstantSeg name :: segs ->
            tmpConsts.Add name
            makeRouteBuilder segs tmpConsts ret
          | seg :: segs ->
            let name =
              match seg with
              | Int64Seg name | IntSeg name | StringSeg name | GuidSeg name -> name
              | ConstantSeg _ -> failwith "err."
            if tmpConsts.Count > 0 then
              let prefix = tmpConsts |> String.concat "/" |> sprintf "\"%s/\""
              tmpConsts.Clear ()
              ret.Add prefix
            let argPart =
              match seg with
              | Int64Seg _ | IntSeg _ -> sprintf "%s.ToString" name
              | GuidSeg _ -> sprintf "%s.ToString(\"D\")" name
              | StringSeg _ -> name
              | ConstantSeg _ -> failwith "err."
            ret.Add argPart
            makeRouteBuilder segs tmpConsts ret
        writer.StartWriteLine <| makeRouteBuilder (klass.Segments) (ResizeArray<_>()) (ResizeArray<_>())
      )

  let dynamicParamTypeName (dynParam: DynamicParam): string =
    match dynParam with
    | Int64Param _ -> "int64"
    | IntParam _ -> "int"
    | StringParam _ -> "string"
    | GuidParam _ -> "Guid"

  let paramListTypeString (paramList: DynamicParam list) (options: RouteCompilationArgs): string =
    let functionTypes = paramList |> List.map dynamicParamTypeName |> ResizeArray

    if functionTypes.Count = 0 || options.InputType then
      let inputTypeName = if options.InputType then "'TContext" else "unit"
      functionTypes.Insert (0, inputTypeName)
    let outputTypeName = if options.ReturnType then "'TReturn" else "unit"
    functionTypes.Add outputTypeName

    String.concat "->" functionTypes

  let notFoundHandlerParams: DynamicParam list = [
    StringParam "verb"
    StringParam "path"
  ]

  let notFoundCtorString (options: RouteCompilationArgs) (inRecord: bool): string =
    let typeString: string = paramListTypeString notFoundHandlerParams options
    let delimiter: string = if inRecord then ":" else "="
    sprintf "notFound%s (%s) option" delimiter typeString

  let renderRecordValues (ctorParams: HandlerCtorParam list) (options: RouteCompilationArgs) (writer: FSharpWriter) =
    writer.StartWrite "{"

    let ctorParams: HandlerCtorParam [] = ctorParams |> Array.ofList
    for i in [0..ctorParams.Length - 1] do
      let ctorParam: HandlerCtorParam = ctorParams.[i]
      let ctorArgs: DynamicParam list = ctorParam.HandlerArgs |> List.map (function | FunctionParam p -> p)

      if i = 0 then
        let spaces: String = new String (' ', writer.IndentSize - 1)
        writer.Write <| sprintf "%s%s: %s" spaces (ctorParam.Name) (paramListTypeString ctorArgs options)
      else
        let spaces: String = new String (' ', writer.IndentSize)
        writer.StartWrite <| sprintf "%s%s: %s" spaces (ctorParam.Name) (paramListTypeString ctorArgs options)
      writer.Write("\n")

    writer.StartWrite <| sprintf "%s%s" (new String (' ', writer.IndentSize)) (notFoundCtorString options true)
    writer.Write (" }")

  let renderCreateFunction (klassName: string) (ctorParams: HandlerCtorParam list) (options: RouteCompilationArgs) (writer: FSharpWriter) =
    let returnTypeString: string =
      if options.InputType && options.ReturnType then klassName + "<_, _>"
      elif options.InputType || options.InputType then klassName + "<_>"
      else klassName

    let notFoundFunctionString: string = paramListTypeString notFoundHandlerParams options
    let functionStartString: string = "static member Router("
    writer.StartWrite functionStartString

    let ctorParams: HandlerCtorParam [] = ctorParams |> Array.ofList
    let functionArgSpaces: String = new String (' ', functionStartString.Length)
    for i in [0..ctorParams.Length - 1] do
      let ctorParam: HandlerCtorParam = ctorParams.[i]
      let ctorArgs: DynamicParam list = ctorParam.HandlerArgs |> List.map (function | FunctionParam p -> p)
      let spaces: string = if i = 0 then "" else functionArgSpaces
      let wr: (string -> unit) = if i = 0 then writer.Write else writer.StartWrite

      wr <| sprintf "%s%s: %s" spaces (ctorParam.Name) (paramListTypeString ctorArgs options)
      writer.Write (",\n")

    writer.StartWrite <| sprintf "%s?notFound: %s" functionArgSpaces notFoundFunctionString
    writer.Write <| sprintf ") : %s = \n" returnTypeString
    using (writer.Indent ()) <| fun _ ->
      writer.StartWrite "{"
      for i in [0 .. ctorParams.Length - 1] do
        let ctorParam = ctorParams.[i]
        if i = 0 then
          let spaces: String = new String (' ', writer.IndentSize - 1)
          writer.Write <| sprintf "%s%s = %s\n" spaces (ctorParam.Name) (ctorParam.Name)
        else
          let spaces: String = new String (' ', writer.IndentSize)
          writer.StartWrite <| sprintf "%s%s = %s\n" spaces (ctorParam.Name) (ctorParam.Name)
      writer.StartWrite <| sprintf "%snotFound = notFound" (new String (' ', writer.IndentSize))
      writer.Write "}"
    
  type RouteNode =
    { EndPoints: EndPoint list
      Children: (NamedRouteSegment * RouteNode) list
      Depth: int }
      static member Empty () = {
        EndPoints = []
        Children = []
        Depth = 0 }
  and EndPoint = {
    Verb: string
    HandlerName: string
    Segments: NamedRouteSegment list
  }

  let routeEndPoint (route: Route): EndPoint = {
    Verb = route.Verb
    HandlerName = (handlerName route)
    Segments = route.RouteSegments
  }

  let rec buildNode (endPoint: EndPoint) (segmentsRemaining: NamedRouteSegment list) (depth: int): RouteNode =
    match segmentsRemaining with
    | [] ->
      {
        EndPoints = [endPoint]
        Children = []
        Depth = depth
      }
    | segment :: segments ->
      let child: (NamedRouteSegment * RouteNode) = segment, (buildNode endPoint segments (depth + 1))
      {
        EndPoints = []
        Children = [child]
        Depth = depth
      }

  let rec addRoute (routeNode: RouteNode) (endPoint: EndPoint) (segmentsRemaining: NamedRouteSegment list) (depth: int): RouteNode =
    match segmentsRemaining with
    | [] ->
      {
        routeNode with EndPoints = routeNode.EndPoints @ [endPoint]
      }
    | segment :: segments ->
      let existingChild =
        routeNode.Children
        |> Seq.tryFind (fun (s, _) -> s = segment)
      match existingChild with
      | None ->
        let newChild: (NamedRouteSegment * RouteNode) = segment, buildNode endPoint segments (depth + 1)
        {
          routeNode with Children = routeNode.Children @ [newChild]
        }
      | Some existingChild ->
        let updatedChildren =
          routeNode.Children
          |> List.map (fun child ->
            if child = existingChild then
              let (segment, routeNode): (NamedRouteSegment * RouteNode) = child
              (segment, (addRoute routeNode endPoint segments (depth + 1)))
            else
              child
          )
        {
          routeNode with Children = updatedChildren
        }

  let buildRouteTree (routes: Route list) =
    let rec addRouteF routeNode routes =
      match routes with
      | route :: routes ->
        let updatedRouteNode = addRoute routeNode (routeEndPoint route) (route.RouteSegments) 0
        addRouteF updatedRouteNode routes
      | [] ->
        routeNode
    addRouteF (RouteNode.Empty ()) routes
   
  let renderNotFoundCall (options: RouteCompilationArgs) (writer: FSharpWriter) =
    let notFoundArgs': string list = [
      "verb"
      "path"
    ]
    let notFoundArgs: string list =
      if options.InputType then "context" :: notFoundArgs'
      else notFoundArgs'
    let argumentString = notFoundArgs |> String.concat ", "
    writer.StartWriteLine <| sprintf "member inline private this.HandleNotFound (%s) = " argumentString

    using (writer.Indent ()) <| fun _ ->
      writer.StartWriteLine <| "match this.NotFound with"
      writer.StartWriteLine <| "| None -> raise (Internal.RouteNotMatchedException (verb, path))"
      let notFoundArgumentString = notFoundArgs |> String.concat " "
      writer.StartWriteLine <| sprintf "| Some notFound -> notFound %s" notFoundArgumentString

  let getBranches (routeTree: RouteNode): (NamedRouteSegment list * EndPoint) seq =
    let rec getBranches' (routeTree: RouteNode) (preSegments: NamedRouteSegment list) =
      seq {
        for endPoint in routeTree.EndPoints do
          yield (List.rev preSegments), endPoint
        for segment, subTree in routeTree.Children do
          yield! (getBranches' subTree (segment :: preSegments))
      }
    getBranches' routeTree []

  let getDynamicParams (segments: NamedRouteSegment list): DynamicParam list =
    segments
    |> List.choose (
      function
      | Int64Seg name -> Some <| IntParam name
      | IntSeg name -> Some <| Int64Param name
      | GuidSeg name -> Some <| StringParam name
      | StringSeg name -> Some <| GuidParam name
      | ConstantSeg _ -> None
    ) 

  let captureEq (segment1: NamedRouteSegment) (segment2: NamedRouteSegment): bool =
    match segment1, segment2 with
    | ConstantSeg a, ConstantSeg b when a = b -> true
    | Int64Seg _, Int64Seg _ -> true
    | IntSeg _, IntSeg _ -> true
    | StringSeg _, StringSeg _ -> true
    | GuidSeg _, GuidSeg _ -> true
    | _ -> false

  let segAssigns (segment: NamedRouteSegment): bool =
    match segment with
    | Int64Seg _
    | IntSeg _
    | GuidSeg _ -> true
    | _ -> false

  let segName (segment: NamedRouteSegment): string =
    match segment with
    | Int64Seg name
    | IntSeg name
    | GuidSeg name
    | StringSeg name
    | ConstantSeg name -> name

  let assignNameClash (segment1: NamedRouteSegment) (segment2: NamedRouteSegment): bool =
    match segAssigns segment1, segAssigns segment2 with
    | true, true when segment1 <> segment2 && segName segment1 = segName segment2 -> true
    | _ -> false

  let genericSegment (segment: NamedRouteSegment) (depth: int): NamedRouteSegment =
    match segment with
    | Int64Seg _ -> Int64Seg (sprintf "int64ArgDepth_%d" depth)
    | IntSeg _ -> IntSeg (sprintf "intArgDepth_%d" depth)
    | StringSeg _ -> StringSeg (sprintf "stringArgDepth_%d" depth)
    | GuidSeg _ -> GuidSeg (sprintf "guidArgDepth_%d" depth)
    | _ -> failwith "err"

  let resolveNameClashes (segment: NamedRouteSegment) (routeTree: RouteNode): (NamedRouteSegment * RouteNode) =
    match routeTree.Children |> List.tryFind (fun (segment2, _) -> assignNameClash segment segment2) with
    | Some (segment2, _) ->
      let newSegment2: NamedRouteSegment = genericSegment segment2 (routeTree.Depth)
      let newSegment: NamedRouteSegment = genericSegment segment (routeTree.Depth)
      let newChildren: (NamedRouteSegment * RouteNode) list =
        routeTree.Children
        |> List.map (fun (tempSegment, c) ->
          if Object.ReferenceEquals (tempSegment, segment2) then
            newSegment2, c
          else
            tempSegment, c
        )
      newSegment, {routeTree with Children = newChildren}
    | None -> segment, routeTree

  let groupNodesByLength (routeTree: RouteNode): (int * RouteNode) list =
    let rec addGroup (routeTree: RouteNode) (segments: NamedRouteSegment list, endPoint: EndPoint) =
      match segments with
      | [] ->
        {
          routeTree with EndPoints = endPoint :: routeTree.EndPoints
        }
      | segment :: segments ->
        let segment, routeTree = resolveNameClashes segment routeTree
        match routeTree.Children |> List.tryFindIndex (fun (segment2, _) -> captureEq segment segment2) with
        | Some index ->
          let segment2, existingChild = routeTree.Children |> List.item index
          let newSegment: NamedRouteSegment =
            if segment = segment2 then segment
            else genericSegment segment (routeTree.Depth)
          let updatedChild: RouteNode = addGroup existingChild (segments, endPoint)
          let updatedChildren: (NamedRouteSegment * RouteNode) list =
            routeTree.Children
            |> List.mapi (fun i child ->
              if i = index then (newSegment, updatedChild)
              else child
            )
          {
            routeTree with Children = updatedChildren
          }
        | None ->
          {
            routeTree with Children = (segment, buildRouteNode (routeTree.Depth + 1, segments, endPoint)) :: routeTree.Children
          }

    and buildRouteNode (startDepth: int, segments: NamedRouteSegment list, endPoint: EndPoint): RouteNode =
      segments
      |> List.rev
      |> List.fold (fun node segment ->
        {
          Children = [segment, node]
          EndPoints = []
          Depth = node.Depth + 1
        })
        {
          Children = []
          EndPoints = [endPoint]
          Depth = startDepth + segments.Length
        }

    let addTree (treeGroup: (int * RouteNode) list) (len: int, routeParts): (int * RouteNode) list =
      match treeGroup |> List.tryFindIndex (fun (n, _) -> n = len) with
      | Some index ->
        let _, node = Seq.item index treeGroup
        let updatedNode: RouteNode = addGroup node routeParts
        treeGroup
        |> List.mapi (fun i (n, node) ->
          if i = index then
            len, updatedNode
          else
            n, node
        )
      | None ->
        let segments, endPoint = routeParts
        let newNode: RouteNode = buildRouteNode (0, segments, endPoint)
        (len, newNode) :: treeGroup

    routeTree
    |> getBranches
    |> Seq.fold
      (fun accumulator routeParts ->
        let segments, _ = routeParts
        let n: int = List.length segments
        addTree accumulator (n, routeParts)
      )
      []

  let inline dbgComment (writer: FSharpWriter) label a =
    let prettyPrint = sprintf "%s\n%A" label a
    for line in prettyPrint.Split ([|"\r\n"; "\n"|], StringSplitOptions.None) do
      writer.StartWriteLine <| sprintf "// %s" line

  let segScore: NamedRouteSegment -> int = function
    | ConstantSeg _ -> 10
    | Int64Seg _ -> 20
    | IntSeg _ -> 30
    | GuidSeg _ -> 35
    | StringSeg _ -> 40

  let noOptDisp =
    {
      new System.IDisposable with
        member y.Dispose () = ()
    }

