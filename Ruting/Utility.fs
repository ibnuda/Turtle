﻿namespace Turtle.Ruting

open System
open System.IO

module Utility =
  type ObjectUtilities() =
    static let idStart = Random().Next(Int32.MaxValue) |> Convert.ToInt64
    static let idGen = new System.Runtime.Serialization.ObjectIDGenerator()

    static member GetInstanceId(o: obj) =
      let id, _ = idGen.GetId(o)
      id + idStart

  let logLock = obj()
  let inline log fmt =
    let cont (s: string) =
      let path = Path.Combine(__SOURCE_DIRECTORY__, "tp.log")
      lock logLock <| fun () ->
        use f = File.Open(path, FileMode.Append)
        use w = new StreamWriter(f)
        fprintfn w "%s" s
    Printf.kprintf cont fmt

  let getTmpFileName folder ext =
    let ran = new Random()
    let rec getFile attemptsLeft =
      let n = ran.Next(Int32.MaxValue)
      let fileName = sprintf "%d.%s" n ext
      let path = Path.Combine(folder, fileName)
      try
        File.Open(path, FileMode.CreateNew).Dispose()
        path
      with
      | :? IOException ->
        if attemptsLeft = 0 then reraise()
        else getFile (attemptsLeft - 1)
    getFile 4
