﻿namespace Turtle.Ruting

module Route =
  type NamedRouteSegment =
  | ConstantSeg of string
  | Int64Seg of string
  | StringSeg of string
  | IntSeg of string
  | GuidSeg of string

  type Route = {
    RouteSegments: NamedRouteSegment list
    Verb: string
    RouteName: string option
  }
