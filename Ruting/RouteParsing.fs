﻿namespace Turtle.Ruting
open Route
open FParsec

module RouteParsing =
  let pIdentifier = identifier (new IdentifierOptions()) 
  let pVerb = many1SatisfyL isAsciiUpper "1 OR more upper case letter (HTTP VERB)"
  let pTypeAnnotation = pchar ':' >>? choice [attempt <| pstring "int64" >>% Int64Seg
                                              attempt <| pstring "int" >>% IntSeg
                                              attempt <| pstring "Guid" >>% GuidSeg
                                              pstring "string" >>% StringSeg ]

  let isNameStart c = isLetter c || c = '_'
  let isNameCont c = isLetter c || isDigit c || c = '_'
  let pName = many1Satisfy2L isNameStart isNameCont "1 or more digit, letters, or underscore"

  let isConstSegChar c =
    isLetter c || isDigit c ||
    match c with
    | '_' | '.' | '_' | '~' | '!'
    | '$' | '&' | ''' | '(' |')'
    | '*' | '+' | ';' | '=' | ':' | '@' -> true
    | _ -> false

  let pConstantSeg = many1Satisfy isConstSegChar |>> ConstantSeg
  let pDyneSeg =
    pName .>>. opt pTypeAnnotation |>> fun (name, annotation) ->
    match annotation with
    | Some segFn -> segFn name
    | None -> Int64Seg name

  let pDynamicPathSeg = between (pchar '{') (pchar '}') pDyneSeg
  let pPathSeg = choice [pConstantSeg; pDynamicPathSeg]
  let pPath = opt (pchar '/') >>. sepEndBy1 pPathSeg (pchar '/')
  let pRouteName = pstringCI "AS" >>. spaces >>. pIdentifier

  let pRoute =
    tuple3
      pVerb
      (spaces >>. pPath)
      (opt (spaces >>? pRouteName))
    |>> fun (verb, segs, routeName) -> {Verb = verb; RouteSegments = segs; RouteName = routeName}

  let pRoutes: Parser<_, unit> = spaces >>. sepEndBy1 pRoute spaces .>> eof

  let inline private testP p s =
    runParserOnString p () "Test" s

  type RouteParseResult = Success of Route list | Failure of string

  let parseRoutes routesStr =
    match runParserOnString pRoutes () "User routes" routesStr with
    | ParserResult.Success (routes, _, _) -> Success routes
    | ParserResult.Failure (message, _, _) -> Failure message
